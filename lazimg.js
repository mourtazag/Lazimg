"use strict"

	function loadImg(imgSrc) {

		return new Promise(function(resolve, reject) {
			const img = new Image()
			
			img.src = imgSrc

			img.onload = function() {
				resolve(img)
			}

			img.onerror = function() {
				reject()
			}
		})

	}

	function Lazimg(nodelist, params) {

		if(nodelist[0].tagName !== "IMG") throw new Error("Nodes provided are not img")

		const options = params || {
			threshold: 0,
			rootMargin: "0px 0px 200px",
		}

		let observer

		// Check IntersectionObserver API support
		if(!('IntersectionObserver' in window)) {

			
			Array.prototype.forEach.call(nodelist, function(node) {

				loadImg(node.dataset.src)
					.then(function(newImg) {

						node.src = newImg.src

						if(options.callback && typeof options.callback === "function") options.callback(node)
					})

			})

		} else {

			observer = new IntersectionObserver(intersected, options)

			Array.prototype.forEach.call(nodelist, function(item) {

				observer.observe(item)

			})
		}


		function intersected(entries) {

			Array.prototype.forEach.call(entries, function(entry) {

				if(entry.isIntersecting) {

					loadImg(entry.target.dataset.src)
						.then(function() {

							entry.target.src = entry.target.dataset.src
							observer.unobserve(entry.target)

							if(options.callback && typeof options.callback === "function") options.callback(entry.target)

						})

				}
			})

		}
	}

	const imgs = document.querySelectorAll(".lazy")

	Lazimg(imgs, {
		rootMargin: "-200px 0px -200px",
		callback: function(entry) {

			entry.classList.add("is-visible")

		}
	})