# Lazimg, the image lazy loader

## Usage

```
<ul class="list-img">
	<li>
	    <img class="lazy" data-src="https://images.unsplash.com/photo-1495572195357-1c8d6b84bc52?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1ac7ad3108db3b6bb42ef34d61e2fee8&auto=format&fit=crop&w=500&q=60" alt="">
	</li>
	<li>
	    <img class="lazy" data-src="https://images.unsplash.com/photo-1499365094259-713ae26508c5?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=26d4766855746c603e3d42aaec633144&auto=format&fit=crop&w=500&q=60" alt="">
    </li>
    <li>
        <img class="lazy" data-src="https://images.unsplash.com/photo-1504976313612-eed10fa2bc11?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5a1eb04528f657ce7b7f97c9da5db1b0&auto=format&fit=crop&w=500&q=60" alt="">
    </li>
    <li>
        <img class="lazy" data-src="https://images.unsplash.com/photo-1508397426994-fcb4d80a41d0?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=550bef85bf7b92a8547fb35d8c523456&auto=format&fit=crop&w=500&q=60" alt="">
    </li>
</ul>

<script src="[...]/lazimg.js"></script>
<script>
    const imgs = document.querySelectorAll(".lazy");
    
    Lazimg(imgs);
</script>
```
- Require IntersectionObserver API. If not available, the images will preload instantly
- Require Promise Polyfill